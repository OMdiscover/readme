# Descriptif du projet

- Auteur : 
    - Florian MUNIER, 
    - Schadrac NOUFE SIE, 
    - Marie Tiffany NGONO NSA, 
    - Ousmane NGOUYAMSA NDAM
- Formation : MS Infrastructures Cloud et DevOps
- Date : 17/03/2023 - 27/03/2023
- Description : Ce projet a pour but de déployer l'application ICD Meteo, créée par la promo MS ICD 2023, dans une infrastructure hébergée sur OpenStack et déployée par Terraform. Un suivit monitoring est également réalisé dans le but d'assurer le bon fonctionnement de l'application.

# Manuel d'utilisation

Ce manuel decris l'ordre d'utilisation des differends dépôt de l'application :

## Dépôt1

Le premier dépôt est intitulé [weather_app](https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/weather_app "weather_app"). Il decrit et présente l'architecture de l'application ICD Meteo, présente la réalisation des tests et intégration continues et la livraison de l'application. 

## Dépôt2

Le second dépôt est intitulé [terrafom](https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/terraform "terrafom"). Il permet de déployer une infrastructure dans OpenStack et d'initialiser le cluster Kuberntes.

## Dépôt3

Le troisième dépôt est intitulé [kubernetes](https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/kubernetes "kubernetes"). Il contient les manifests de l'application ICD Meteo ainsi que ceux des outils de monitoring Prométheus, Zabbix et Grafana, déployés dans le cluster Kubernetes.

## Dépôt4

Le quatrième dépôt est intitulé [ansible](https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/ansible "ansible"). Il permet de déployer via un playbook, l'application ICD Meteo, ainsi que les outils de monitoring Prométheus, Zabbix et Grafana, sur un cluster kubernetes d'une infrastructure générée via le projet terraform.

> Chaque dépôt possède son propre README.md.

# Accès à l'application

> Les adresses IP se trouvents dans le fichier "hosts.ini" généré par Terraform.

```sh
http://<MASTER_PUB_IP>
```

# Accès aux outils de monitoring

## Prométheus

```sh
http://<MASTER_PUB_IP>:30000
```

## Zabbix

```sh
http://<ZABBIX_SERVER_PUB_IP>/zabbix
```

## Grafana

```sh
http://<MASTER_PUB_IP>:32000
```

# Paramétrage des outils de monitoring

## Zabbix

* Lors de la première connexion, il faut valider les confguration de connection à la base de données. L'utilisateur zabbix a pour login et mot de passe "zabbix".

* Vous pouvez appeler le serveur "zabbix-server".

* Une fois paramétré, la page de connexion s'affiche. Entrer le couple login : mot de passe Admin : zabbix.

* Création d'un groupe pour l'application :

    - Sélectionner "Collecte de données"
    - Sélectionner "Groupes d'hôtes"
    - Sélectionner "Créer un groupe d'hôtes"
    - Remplir le champ demandé (ex : weather)
    - Sélectionner "Ajouter"

* Enregistrer automatiquement les hôtes :

1 - Collecte de données

    - Sélectionner "Collecte de données" / "Découverte"
    - Sélectionner "Créer une règle de découverte"
    - Renseigner :
        - le nom de la règle
        - la plage d'IP : 192.168.1.1-254
        - l'intervalle d'actualisation en seconde ou en minute pour une actualisation rapide
        - vérifications : http et agent Zabbix (Intervalle du port : 10050 / Clé : system.uname)
    - Sélectionner "Ajouter"

2 - Alertes :

    - Sélectionner "Alertes" / "Action" / "Actions d'enregistrement automatiquement"
    - Sélectionner "Créer une action"
    - Renseigner le nom de l'action et ajouter les conditions :
        - type : nom de l'hôte / Valeur : bastion
        - type : nom de l'hôte / Valeur : master
        - type : nom de l'hôte / Valeur : node
    - Renseigner les opérations suivantes : 
        - lier aux modèles: Linux by Zabbix agent active
        - activer hôte
        - ajouter hôte
        - ajouter aux groupes d'hôtes: weather
    - Sélectionner "Ajouter"

> Vous pouvez maintenant visualiser les métrics dans "Surveillance" / "Dernières données" et accéder au tableau de bord. 

> Les hôtes sont visibles dans "Surveillance" / "Hôtes"

## Grafana

### Ajouter le plugin Zabbix

* Allez dans l’onglet “Configuration” (à gauche), cliquez sur “Plugins”

* Cherchez et sélectionner “Zabbix”, et cliquez sur “Installer”

* Raffraichir la page du navigateur et cliquez sur “Enable”

* Allez dans l’onglet “Configuration” (à gauche), cliquez sur “Data Sources”

* Cliquez sur “Add data source”

* Cherchez “Zabbix” et sélectionnez-le

* Dans le champs “Name” : Zabbix

* Dans le champs “URL” : http://<ZABBIX_SERVER_PUB_IP>/zabbix/api_jsonrpc.php

* Cochez Basic Auth

* Dans “Basic Auth Details” : rentrez votre utilisateur et le mot de passe (Admin : zabbix)

* Dans “Zabbix API details” : rentrez votre utilisateur et le mot de passe (Admin : zabbix)

* Cliquez sur “Save & Test“

### Dashboards

* Après connexion à l'interface graphique de Gafana, il faut ajouter les dasboaords comme suit :

* Téléchargez d'abord le fichier JSON du dashboard que vous souhaitez:

```sh
https://grafana.com/grafana/dashboards/15172-node-exporter-for-prometheus-dashboard-based-on-11074/ # Pour les métrics sur les noeuds du cluster kubernetes

https://grafana.com/api/dashboards/12708/revisions/1/download # Pour les métrics au niveau du serveur NGINX

https://grafana.com/grafana/dashboards/7249-kubernetes-cluster/ # Pour les métriques des PODS

https://grafana.com/api/dashboards/9614/revisions/1/download # Pour les métrics au niveau de l'NGINX Ingress Controllers

https://grafana.com/api/dashboards/17014/revisions/1/download #Pour les métrics du zabbix-server
```

* Importez les dashboards dans Grafana au niveau de Dashboards / Import

* Faites glisser le fichier JSON dans la zone d'upload

* Sélectionnez la data source

* Valider



